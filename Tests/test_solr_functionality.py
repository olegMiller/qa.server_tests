import time
from datetime import datetime, timedelta
from Utilities.SolrUtility import SolrUtility


class TestExecutorSolr(object):
    @classmethod
    def setup_class(cls):
        cls.solr = SolrUtility()

    # def test_create_running_protector(self):
    #     begin_number = self.solr.get_number_of_documents_in_collection("running_protector", "*:*")
    #     # Timestamp format : "2018-06-17T16:19:35.314Z"
    #     # date_format = "%Y-%m-%dT%H:%M:%S.%fZ"
    #     time_stamp = time.strftime("%Y-%m-%dT%H:%M:%S", time.localtime())
    #     solr_injection_file = self.solr.update_json('..\Resources\protector.json', 'keepalive_timestamp_dt',
    #                                                 time_stamp, 'running')
    #     assert 'Solr_injection_' in solr_injection_file
    #
    #     # TODO: Send the request
    #     self.solr.insert_record("running_protector", solr_injection_file)
    #     after_number = self.solr.get_number_of_documents_in_collection("running_protector", "*:*")
    #     assert after_number - begin_number == 1
    #     # pass

    def test_create_unknown_protector(self):
        begin_number = self.solr.get_number_of_documents_in_collection("running_protector", "*:*")
        # Timestamp format : "2018-06-17T16:19:35.314Z"
        date_string = datetime.now()
        past_date = date_string - timedelta(days=10)
        time_stamp = datetime.strftime(past_date, "%Y-%m-%dT%H:%M:%SZ")
        solr_injection_file = self.solr.update_json('..\Resources\protector.json', 'keepalive_timestamp_dt'
                                                    , time_stamp, 'unknown')
        assert 'Solr_injection_' in solr_injection_file

        # Send the request
        self.solr.insert_record("running_protector", solr_injection_file)
        after_number = self.solr.get_number_of_documents_in_collection("running_protector", "*:*")
        # Verify 1 entry has been added
        #assert after_number - begin_number == 1
        pass

    # def test_reading(self):
    #     number = self.solr.get_number_of_documents_in_collection("running_protector", "*:*")
    #     assert number > 0

    @classmethod
    def teardown_class(cls):
        pass
