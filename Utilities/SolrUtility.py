from urllib2 import *
import requests
import json
import string
import pysolr


class SolrUtility(object):

    from urllib2 import Request
    from urllib2 import urlopen
    import urllib2

    def insert_record(self, collection, file_name):
        json_file = open('..\/Runtime_Files\/' + file_name, 'r')
        # payload = json_file.readlines()
        payload = json.load(json_file)
        url = 'http://localhost:8983/solr/' + collection + '/update/json?commit=true'
        headers = {'content-type': 'application/json'}

        # implementing python requests library
        #response = requests.post(url, data=json.dumps([payload]), headers=headers)
        url = 'http://localhost:8983/solr/' + collection + '/update/json?commit=true'
        req = self.urllib2.Request(url, data=json.dumps([payload]), headers={'Content-Type': 'application/json'})

        # Assert that response code is 200 OK
        #assert response.status_code == 200

        # Assert that response content header is 0 --> indicates success
        # assert response.content == {"responseHeader":{"status":0,"QTime":234}}
        #assert json.loads(response.content)['responseHeader']['status'] == 0

        json_file.close()

    def query_collection(self, collection=None, query=None):
        connection = urlopen('http://localhost:8983/solr/' + collection + '/select?q=' + query + '&wt=python')
        response = eval(connection.read())
        print response['response']['numFound'], "documents found."

    def get_number_of_documents_in_collection(self, collection=None, query=None):
        connection = urlopen('http://localhost:8983/solr/' + collection + '/select?q=' + query + '&wt=python')
        response = eval(connection.read())
        return response['response']['numFound']
        pass

    def update_json(self, file_name, key, value,type):
        """
        Description: modifies the value for a given key in a json file
        :param file_name:  template file name
        :param key:  json key
        :param value: new json value
        :return: modified json file name
        """
        json_file = open(file_name, "r")  # Open the JSON file for reading
        data = json.load(json_file)  # Read the JSON into the buffer
        json_file.close()  # Close the JSON file

        # Working with buffered content
        tmp = data[key]
        data[key] = value

        # Create new filename
        modified_json = '..\/Runtime_Files\/Solr_injection_' + type + '_' + time.strftime("%d%m%Y_%H%M%S") + '.json'
        # Save our changes to JSON file
        json_file = open(modified_json, "w+")
        json_file.write(json.dumps(data))
        json_file.close()

        # Retun the created file name
        return modified_json

    # Protector collection
    def insert_running_protector_data(self):
        pass

    def modify_protector_data(self):
        pass

    def purge_protector_data(self):
        pass

    def query_protector_data(self):
        pass

    # Attacks Collection
    def insert_attack_data(self):
        pass

    def modify_attack_data(self):
        pass

    def purge_attack_data(self):
        pass

    def query_attack_data(self):
        pass

    # Agents Collection
    def insert_agent_data(self):
        pass

    def modify_agent_data(self):
        pass

    def purge_attack_data(self):
        pass

    def query_attack_data(self):
        pass

    # Current Collection
    def insert_current_data(self):
        pass

    def modify_current_data(self):
        pass

    def purge_current_data(self):
        pass

    def query_current_data(self):
        pass

